<!DOCTYPE html>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Bootstrap Example</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
    </head>
    <body>

        <div class="container">
            <h3>Forget Password!</h3>
            <!-- Trigger the modal with a button -->
            <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#myModal">Here</button>

            <!-- Modal -->
            <div class="modal fade" id="myModal" role="dialog">
                <div class="modal-dialog">
                    <form action="#html">
                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Forget Password!</h4>
                            </div>
                            <div class="modal-body">

                                <p>Enter Email Here</p>
                                <input type="email" class="form-control" autofocus="autofocus" id="email" placeholder="Enter email" name="tx_reEmail">
                                <p>Enter NIC Here</p>
                                <input type="text" class="form-control" placeholder="Enter NIC Number" aria-describedby="sizing-addon1" name="pa_reNic">

                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal"  onclick="ClearFields()">Submit</button>

                            </div>

                        </div>
                    </form>
                </div>
            </div>

        </div>
        <script type="text/javascript">
            function ClearFields() {

                document.getElementById("tx_firstname").value = "";
                document.getElementById("tx_lastname").value = "";
            }
        </script>

    </body>
</html>
