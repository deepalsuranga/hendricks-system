<!DOCTYPE html>
<!--

-->
<html>
    <head>
        <meta charset="UTF-8">
        <link rel="shortcut icon" href="images/icon.png">
        <title>Hendricks-system</title>

        <!--jquary start--> 
        <script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
        <script src="jqury/jquery-1.11.3.min.js"></script>
        <!--jquary end-->
        <!--css start-->
        <link href="src/css/bootstrap-theme.css" rel="stylesheet">
        <link href="src/css/bootstrap-theme.min.css" rel="stylesheet">
        <link href="src/css/bootstrap.css" rel="stylesheet">
        <link href="src/css/bootstrap.min.css" rel="stylesheet">
        <link href="css/index.css" rel="stylesheet">

        <!-- web's-->
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">

        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">
        <!-- css end-->
    </head>
    <body>
        <div class="container">

            <div class="jumbotron">
                <img src="images/b-logo.png" width="78" height="61" alt="b-logo"/>

                <h2>Hendricks Management System</h2>
                <?php
                echo "Today is " . date("Y/m/d") . "<br>";
                echo "Today is " . date("Y.m.d") . "<br>";
                echo "Today is " . date("Y-m-d") . "<br>";
                echo "Today is " . date("l");
                ?>
                <hr />
                <p>Our Business Manager </p>


            </div>
            <!--  -->
            <!--  -->
            <div class="row">
                <div class="col-sm-4"></div>
                <div class="col-sm-4">
                    <form role="form" name="loginhere" action="home.php" method="GET">
                        <div class="form-group">
                            <label for="email">Email:</label>
                            <input type="email" class="form-control" autofocus="autofocus" id="email" placeholder="Enter email" name="tx_email">
                        </div>
                        <div class="form-group">
                            <label for="pwd">Password:</label>
                            <input type="password" class="form-control" id="pwd" placeholder="Enter password" name="ps_password">
                        </div>
                        <div class="checkbox">
                            <label><input type="checkbox">Remember me</label>
                        </div>
                        <button type="submit" class="btn btn-default">Login Here</button>
                    </form>
                </div>
                <div class="col-sm-4"></div>
            </div>
            <!--  -->
            <!--  -->
            <br />
            You haven't account?
            <a href="register.php">Register Here</a>

            <!-- java script start function-->
            <script type="text/javascript">
                function changer() {
                    document.getElementById(bt_loginHere).innerHTML = '<button type="button" class="btn btn-success" name="bu_login">Login Here</button>';
                }
                function ClearFields() {

                    document.getElementById("textfield1").value = "";
                    document.getElementById("textfield2").value = "";
                }
            </script>
            <!-- java script start-->
            <script src="src/js/bootstrap.min.js"></script>
            <script src="src/js/bootstrap.js"></script>
            <script src="src/js/npm.js"></script>
            <!-- java script end-->
            <!-- web java script start-->
            <!-- Latest compiled and minified JavaScript -->
            <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
            <!-- web java script end-->
        </div>
        <footer>
            <p class="center-block">
            © 2010-<?php echo date("Y")?> hendricks company
            </p>
        </footer>
    </body>
</html>
