<!DOCTYPE html>
<!--

-->
<html>
    <head>
        <meta charset="UTF-8">
        <link rel="shortcut icon" href="images/icon.png">
        <title>Hendricks-system</title>

        <!--jquary start-->
        <script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
        <script src="jqury/jquery-1.11.3.min.js"></script>
        <!--jquary end-->
        <!--css start-->
        <link href="src/css/bootstrap-theme.css" rel="stylesheet">
        <link href="src/css/bootstrap-theme.min.css" rel="stylesheet">
        <link href="src/css/bootstrap.css" rel="stylesheet">
        <link href="src/css/bootstrap.min.css" rel="stylesheet">
        <link href="css/index.css" rel="stylesheet">

        <!-- css web's-->
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">

        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">
        <!-- css end-->
    </head>
    <body>
        <div class="container">

            <div class="jumbotron">
                <img src="images/b-logo.png" width="78" height="61" alt="b-logo"/>
                <h2>Hendricks Management System</h2>
                <hr />
                <p>Our Business Manager </p> 

            </div>
            <!--  -->
            <!--  -->
            <form name="loginhere" action="#html" method="GET">
                <div class="row">
                    <div class="col-xs-1"></div>
                    <div class="col-xs-3">
                        <h3>First Name</h3>
                        <div class="input-group input-group-btn">
                            <input type="text" class="form-control" autofocus="autofocus" placeholder="Enter Firstname" aria-describedby="sizing-addon1" name="tx_firstname">
                        </div>
                        <h3>E-mail</h3>
                        <div class="input-group input-group-btn">
                            <input type="text" class="form-control" placeholder="Enter Email" aria-describedby="sizing-addon1" name="tx_email">
                        </div>
                        <h3>Password</h3>
                        <div class="input-group input-group-btn">
                            <input type="password" class="form-control" placeholder="Enter Password" aria-describedby="sizing-addon1" name="pa_upass">
                        </div>
                        <br/>
                        <button type="submit" class="btn btn-default">Login Here</button>
                    </div>
                    <div class="col-xs-3">
                        <h3>Last Name</h3>
                        <div class="input-group input-group-btn">
                            <input type="text" class="form-control" placeholder="Enter Lastname" aria-describedby="sizing-addon1" name="tx_lastname">
                        </div>                      
                        <h3>NIC No</h3>
                        <div class="input-group input-group-btn">
                            <input type="text" class="form-control" placeholder="Enter NIC Number" aria-describedby="sizing-addon1" name="pa_upassConform">
                        </div>
                        <h3>Conform Password</h3>
                        <div class="input-group input-group-btn">
                            <input type="password" class="form-control" placeholder="Re-Enter Password" aria-describedby="sizing-addon1" name="pa_upassConform">
                        </div>

                    </div>
                </div>
            </form>
            <script type="text/javascript">
                
            </script>
        </div>
    </div>
</div>
<!-- java script start-->
<script src="src/js/bootstrap.min.js"></script>
<script src="src/js/bootstrap.js"></script>
<script src="src/js/npm.js"></script>
<!-- java script end-->
<!-- web java script start-->
<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
<!-- web java script end-->
</body>
</html>
